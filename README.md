# tecaj.go 

Skida trenutni tecaj sa HNB stranica te ga sprema u JSON formatu.

Struktura podataka je sljedeca:

    type CurrencyData struct {
	    Header Header
	    Items  []Currency
    }

Header sadrzi podatke iz prve linije formatiranog zapisa.
Items sadrzi podatke o tecaju - tecajnu listu.

# server.go

Servira JSON datoteku kreiranu sa tecaj.go. 

API: 
    
    /header - zapis headera
    /list - tecajna lista

