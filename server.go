package main

/*

server.go - serve currency list JSON file
dprelec, 2014-11-12

*/

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

type Date struct {
	Day   string
	Month string
	Year  string
}

type Header struct {
	CurrencyNum int
	CreateDate  Date
	ApplyDate   Date
	RecNum      int
}

type Currency struct {
	Num    int
	Name   string
	Unit   int
	Buy    float32
	Median float32
	Sell   float32
}

type CurrencyVal struct {
	Val string
}

type CurrencyData struct {
	Header Header
	Items  []Currency
}

func (h Header) String() string {
	return fmt.Sprintf(
		"Tecaj br: %d\nIzrada: %s\nPrimjena: %s\nBr. slogova: %d",
		h.CurrencyNum, h.CreateDate, h.ApplyDate, h.RecNum)
}

func (d Date) String() string {
	return fmt.Sprintf("%s.%s.%s.", d.Year, d.Month, d.Day)
}

func (c Currency) String() string {
	return fmt.Sprintf("%d %s = %f HRK", c.Unit, c.Name, c.Median)
}

var currency CurrencyData

func ShowHeader(w http.ResponseWriter, req *http.Request) {
	h := fmt.Sprintf("%s", currency.Header)
	io.WriteString(w, h)
}

func ShowCurrencyList(w http.ResponseWriter, req *http.Request) {
	list := fmt.Sprintf("%s", currency.Items)
	io.WriteString(w, list)
}

func main() {
	fn := flag.String("file", "", "Specify input file.")
	flag.Parse()

	jsonData, err := ioutil.ReadFile(*fn)
	if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal(jsonData, &currency)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Serving", *fn, "on localhost:8002")

	http.HandleFunc("/header", ShowHeader)
	http.HandleFunc("/list", ShowCurrencyList)

	err = http.ListenAndServe(":8002", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
