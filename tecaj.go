package main

/*

tecaj.go - fetch and parse currency list, save as JSON
dprelec, 2014-11-09

*/

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

// basic date type
type Date struct {
	Day   string
	Month string
	Year  string
}

// Header defines basic information about currency data:
//   - CurrencyNum - number of currency list issued
//   - CreateDate - date of list creation
//   - ApplyDate - date of application
//   - RecNum - number of subsequent records
type Header struct {
	CurrencyNum int
	CreateDate  Date
	ApplyDate   Date
	RecNum      int
}

// Currency defines single currency list record line.
//   - Num - number of currency item
//   - Name - name of currency item
//   - Unit - unit for base calucation
//   - Buy - currency buying value
//   - Median - currency median value
//   - Sell - currency selling value
type Currency struct {
	Num    int
	Name   string
	Unit   int
	Buy    float32
	Median float32
	Sell   float32
}

var ErrNoResource = errors.New("Resource does not exist.")

// String representation of currency amount value.
type CurrencyVal struct {
	Val string
}

// CurrencyData is composed of Header and a list of Items.
type CurrencyData struct {
	Header Header
	Items  []Currency
}

// String representation of currency line.
func (h Header) String() string {
	return fmt.Sprintf(
		"Tecaj br: %d\nIzrada: %s\nPrimjena: %s\nBr. slogova: %d",
		h.CurrencyNum, h.CreateDate, h.ApplyDate, h.RecNum)

}

// String representation of a date.
func (d Date) String() string {
	return fmt.Sprintf("%s.%s.%s.", d.Year, d.Month, d.Day)
}

// String representation of Currency item.
func (c Currency) String() string {
	return fmt.Sprintf("%d %s = %f HRK", c.Unit, c.Name, c.Median)
}

// Convert string representation of currency value to integer.
func (str CurrencyVal) ToInt32() int {
	num_parsed, err := strconv.ParseInt(str.Val, 10, 32)
	if err != nil {
		log.Fatal(err)
	}
	return int(num_parsed)
}

// Convert string representation of currency value to float32.
func (str CurrencyVal) ToFloat32() float32 {
	num := strings.Replace(str.Val, ",", ".", 1)
	f, err := strconv.ParseFloat(num, 32)
	if err != nil {
		log.Fatal(err)
	}
	return float32(f)
}

// Convert CurrencyData to JSON format.
func (c CurrencyData) ToJSON() []byte {
	jsonData, err := json.Marshal(c)
	if err != nil {
		log.Fatal(err)
	}
	return jsonData
}

// Create currency header data.
func NewHeader(line string) Header {
	num := CurrencyVal{line[0:3]}.ToInt32()
	rec := CurrencyVal{line[19:21]}.ToInt32()
	return Header{num, NewDate(line[3:11]), NewDate(line[11:19]), rec}
}

// Create date from a string.
func NewDate(date string) Date {
	d := date[0:2]
	m := date[2:4]
	y := date[4:8]
	return Date{d, m, y}
}

// Create currency item.
func NewCurrency(line string) Currency {
	fields := strings.Fields(line)
	valuta := fields[0]
	name := valuta[3:6]

	num := CurrencyVal{valuta[0:3]}.ToInt32()
	units := CurrencyVal{valuta[6:9]}.ToInt32()
	buy := CurrencyVal{fields[1]}.ToFloat32()
	median := CurrencyVal{fields[2]}.ToFloat32()
	sell := CurrencyVal{fields[3]}.ToFloat32()

	return Currency{num, name, units, buy, median, sell}
}

// Fetch currency data from uri.
func FetchCurrency(uri string) (string, error) {
	res, err := http.Get(uri)
	if err != nil {
		log.Fatal(err)
	}
	if res.StatusCode != 200 {
		return "", ErrNoResource
	}
	currency, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
	return string(currency), nil
}

// Create CurrencyData from raw strings.
func ProcessCurrency(currencyFile string) CurrencyData {
	lines := strings.Split(currencyFile, "\n")

	header := NewHeader(lines[0])
	items := make([]Currency, header.RecNum, header.RecNum)

	idx := 0
	for _, line := range lines[1:] {
		if len(line) > 0 {
			items[idx] = NewCurrency(line)
			idx++
		}
	}

	return CurrencyData{header, items}
}

// SaveCurrency CurrencyData to a file as JSON representation.
func SaveCurrency(jsonFile string, currencyData CurrencyData) string {
	fh, err := os.Create(jsonFile)
	if err != nil {
		log.Fatal(err)
	}

	fh.WriteString(string(currencyData.ToJSON()))
	fh.Close()
	return jsonFile
}

const (
	sqlSelectProps    = "SELECT * from currency_props WHERE name=$1 AND num=$2"
	sqlInsertProps    = "INSERT INTO currency_props (name, num) VALUES($1, $2) RETURNING(id)"
	sqlInsertCurrency = `INSERT INTO currency_list (currency_id, buying, selling, median, unit) VALUES($1, $2, $3, $4, $5)`
)

func SaveCurrencyToDatabase(currencyData CurrencyData) {
	connStr := "user=etranet password=etranet123 dbname=tecaj"
	db, err := sqlx.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	type Props struct {
		Id   int    `db:"id"`
		Name string `db:"name"`
		Num  int    `db:"num"`
	}

	for _, v := range currencyData.Items {
		p := []Props{}
		err := db.Select(&p, sqlSelectProps, v.Name, v.Num)
		if err != nil {
			log.Fatal(err)
		}

		var id []int
		if len(p) == 0 {
			db.Select(&id, sqlInsertProps, v.Name, v.Num)
		}

		fmt.Println(id[0])
		db.MustExec(sqlInsertCurrency, id[0], v.Buy, v.Sell, v.Median, v.Unit)
	}
}

func main() {
	baseURI := "http://www.hnb.hr/tecajn/f%s.dat"
	y, m, d := time.Now().Date()
	y -= 2000
	baseDate := fmt.Sprintf("%02d%02d%d", d, m, y)
	uri := fmt.Sprintf(baseURI, baseDate)

	currencyTable, err := FetchCurrency(uri)
	var currencyData CurrencyData
	if err != nil {
		log.Fatal(err)
	}

	fjson := fmt.Sprintf("f%s.json", baseDate)
	currencyData = ProcessCurrency(currencyTable)
	SaveCurrency(fjson, currencyData)
	SaveCurrencyToDatabase(currencyData)
	fmt.Println("Currency data saved to", fjson)
}
